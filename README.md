# Note Taking App (Technical assessment for Tyres On The Drive)

[Try it out here](https://stackblitz.com/edit/angular-ky3ouh)

## Decisions Made

* Notes limited to 500 characters to allow variable length from a few words to whole paragraphs
* Notes saved to local storage in order to persist across page loads
* Notes can be deleted individually
* Application built using Angular 6 and Bootstrap 4 due to personal familiarity with these frameworks
* Performance tested in Chrome. Page loaded in 750ms
* Development time was approximately 6 hours

## Additional Features

* Notes can be edited, sorted and categorised
* Note value and category are required fields
* Notes are colour-coded by category