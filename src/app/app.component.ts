import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Note } from './note.class';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  notes: Note[] = [];
  defaultCategory: any = "0";
  categories: any[] = [
    { key: "1", value: "Appointment" },
    { key: "2", value: "Random Musing" },
    { key: "3", value: "To-do" }
  ];

  ngOnInit() {
    // get all notes from local storage and push to notes array  
    for (let i = 0; i < localStorage.length; i++){
      let key = localStorage.key(i);
      let obj = JSON.parse(localStorage.getItem(key));

      // only include note-taking-app notes from local storage
      if (obj.type === "note-taking-app") {
        let note: Note = {
          key: key,
          obj: obj
        }
        this.notes.push(note);
      }
    }
    this.sortBy("keydesc");
  }

  sortBy(sortType: string) {
    // sort notes based on 
    switch (sortType) {
      case 'keyasc':
        this.notes.sort((a, b) => a.key - b.key);
        break;
      case 'keydesc':
        this.notes.sort((a, b) => b.key - a.key);
        break;
      case 'catasc':
        this.notes.sort((a, b) => a.obj.category - b.obj.category);
        break;
      case 'catdesc':
        this.notes.sort((a, b) => b.obj.category - a.obj.category);
    }
  }

  createNote(obj: NgForm): void {
    // create note from form object, push to notes array, add to local storage and reset form object
    let key: string = this.notes.length.toString();

    let note: Note = {
      key: key,
      obj: {
        type: 'note-taking-app',
        category: obj.value.category,
        value: obj.value.note
      },
    }
    this.notes.unshift(note);
    this.resetNote(obj);

    localStorage.setItem(key, JSON.stringify(note.obj));
  }

  editNote(key: string, obj: NgForm): void {
    // edit note from form object
    let note: Note = {
      key: key,
      obj: {
        type: 'note-taking-app',
        category: obj.value.category,
        value: obj.value.note
      },
    }
  
    localStorage.setItem(key, JSON.stringify(note.obj));
  }

  deleteNote(note: Note): void {
    // delete note and remove from local storage
    var index = this.notes.indexOf(note);
    if (index > -1) this.notes.splice(index, 1);
    localStorage.removeItem(note.key);
  }

  resetNote(obj: NgForm): void {
    // reset note
    obj.reset();
    this.defaultCategory = "0";
  }
}